<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://wordpress.org/support/article/editing-wp-config-php/
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define( 'DB_NAME', 'trp-db' );

/** MySQL database username */
define( 'DB_USER', 'trp-user' );

/** MySQL database password */
define( 'DB_PASSWORD', '@e1enu3Rls8Asw3fogaD' );

/** MySQL hostname */
define( 'DB_HOST', 'localhost' );

/** Database Charset to use in creating database tables. */
define( 'DB_CHARSET', 'utf8' );

/** The Database Collate type. Don't change this if in doubt. */
define( 'DB_COLLATE', '' );

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'S5.Tm|]g2jUF[x,&V^N:CUBCsr0c6xhd03}~0mw+[:Z[?h~E>fJgkK(I]B,X@+cU');
define('SECURE_AUTH_KEY',  'w{b;pP^*%Ye60wdx8dA[A[+=UUv<*Z@?fwUqRm&Yac#dZ13WWa uJ8#reXJL-H%d');
define('LOGGED_IN_KEY',    '+dH3~1A+f$Z`RW_)oVy)tSQu*<a52>0ka^5I7#;}bB5d~2;/]1[~p-xUSNPoF$#g');
define('NONCE_KEY',        'eoz>im-Y/Z;;lr|--3PiQi(T`(G7bikW+]nb&H/+^7@mbJ}h<;|2A3n71w*J},[G');
define('AUTH_SALT',        'T Xr*8C{{A={e?M#+H$+DWBrQoII9msXp@&=DNpB.T0r.j+{=_gi/_le;)7WA^oU');
define('SECURE_AUTH_SALT', '3wXx5C:M5Qo~KIfXc,Hj$7zeiwVA]HTJd4_3 g+i|L6Ffx{|)8 >`-lo;HobgB|(');
define('LOGGED_IN_SALT',   '*noFn+d`^;8:}7d<_qT6A^p?RCuS,3FT>M#jmu`(WjF:L8,_e|*3!D`1>WEQ~h}t');
define('NONCE_SALT',       'BL}9tHH_rfC/Z=RIPoZ1K5qU+22+!#y(Yt8{45-;+Q|yG- l%8-p}<V4a:6N4 ]E');
/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix = 'trp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the documentation.
 *
 * @link https://wordpress.org/support/article/debugging-in-wordpress/
 */
define( 'WP_DEBUG', false );

/* That's all, stop editing! Happy publishing. */

/** Absolute path to the WordPress directory. */
if ( ! defined( 'ABSPATH' ) ) {
	define( 'ABSPATH', __DIR__ . '/' );
}

/** Sets up WordPress vars and included files. */
require_once ABSPATH . 'wp-settings.php';
